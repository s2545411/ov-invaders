# Changelog
This file contains the changelog beginning from week 7 ( as specified in the module guide )
## Week 7
### 01.06
* Added boss
* Added boss shots (bouncing errors)
* Added boss health bar
### 02.06
* Added new enemy - ladybug shooting 404s
* Added a high scores screen on the player's death
### 03.06
* Added a settings icon and made it pause the game on pressing
### 05.06
* Changes to the input UI
* Refactored the player into a separate class
### 06.06
* Added Java power-up, which causes the player to shoot three coffee cups that bounce
* Added C++ power-up, which causes the player to shoot SMG-like 0s and 1s
* Added Python power-up, which causes the player to shoot two snakes with crossing paths that penetrate through enemies
## Week 8
In week 8 we were mainly focused on final exams, and we were making music and sound effects that take longer to complete. No feature was completed
## Week 9
### 18.06
* Added sound effects
* Added music
###  21.06
* Added volume sliders in settings
* Added restart button in settings
## Week 10
### 23.06
* Fixed dependencies for deployment on Previder
### 24.06
* Added input character white-listing
### 25.06
* Added tips into the input-form
### 26.06
* Changed input UI scaling to fix issues with safari
### 27.07
* Adding files according to the specified git repository structure from the module guide