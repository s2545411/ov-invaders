class Explosion extends Phaser.GameObjects.Sprite{
    /**
     * creates an explosion animation in a given place
     * @param scene the scene in which the animation will be added
     * @param x the x coordinate of the explosion
     * @param y the y coordinate of the explosion
     * @param scale the scale of the explosion
     */
    constructor(scene, x, y, scale){
        super(scene, x, y, "explosion");
        scene.add.existing(this);
        this.setScale(scale);
        this.play("explode");
    }
}