let highscore;
let oldTransform;
let paused = false;

class Scene2 extends Phaser.Scene {
    constructor() {
        super("playGame");
    }

    create() {
        // Because of using a DOMElement (the ui in scene3) Phaser creates a
        // div with css properties which covers the game and makes it
        // impossible to drag the player, setting the scale of this covering
        // to 0 for now fixes that issue
        this.scale.updateBounds();
        let div1 = document.getElementsByTagName('div');
        oldTransform = div1[0].style.transform;
        div1[0].style.transform = "scale(0,0)";

        this.bugDeadSound = this.sound.add("bugDead"); // done
        this.bugShotSound = this.sound.add("bugShoot"); // done
        this.cShotSound = this.sound.add("c++shot"); // done
        this.clickSound = this.sound.add("click");
        this.errorSound = this.sound.add("errorShot"); // done
        this.gameOverSound = this.sound.add("gameOver"); // done
        this.hurtBossSound = this.sound.add("hurtBoss"); // done
        this.hurtPlayerSound = this.sound.add("hurtPlayer"); // done
        this.pauseSound = this.sound.add("pause"); // done
        this.killBossSound = this.sound.add("killBoss"); // done
        this.shotNormalSound = this.sound.add("shotNormal"); // done
        this.snakeSound = this.sound.add("snake"); // done
        this.powerupSound = this.sound.add("powerUp");
        this.unPauseSound = this.sound.add("unpause"); // done
        this.winSound = this.sound.add("win"); // done
        this.cupSound = this.sound.add("cup"); // done
        this.loudness = 0.5;
        this.musicVolume = 0.2;

        // Initializing the background and setting it as a tile sprite so
        // that it can move endlessly
        this.background = this.add.tileSprite(0, 0, config.width, config.height, "background");
        this.background.setOrigin(0, 0);


        this.player = new Player(this);

        // creating a scoreLabel and setting it's depth so that it is on top
        this.scoreLabel = this.add.bitmapText(3, 3, "pixelFont", "SCORE 000000", 16);
        this.scoreLabel.depth = 10;

        this.scores = [];

        this.settings = this.physics.add.sprite(11, config.height - 11, "gear").setInteractive()
            .on('pointerdown', () => this.pauseUnpause());
        this.settings.setVisible(false);
        // calling cofigure() which will configure other variables
        this.configure();
    }

    pauseUnpause() {
        if (!paused) {
            this.pauseSound.play({volume: this.loudness});
            this.sound.pauseAll();
            this.dim(0.3);
            this.resumeButton = this.add.image(config.width / 2, config.height * 1 / 5-7,
                "resumeButton")
                .setInteractive()
                .on('pointerdown', () => this.pauseUnpause());
            this.resumeButton.depth=11;
            // this.timeUntilFire=this.nextFire-this.time.now;
            this.timeUntilEnemy = this.nextEnemy - this.time.now;
            this.timeUntilBoss = this.bossTimeStamp - this.time.now;
            this.player.pause();
            for (let i = 0; i < this.enemyBeetles.getChildren().length; i++) {
                let beetle = this.enemyBeetles.getChildren()[i];
                beetle.pause();
            }
            for (let j = 0; j < this.bugShots.getChildren().length; j++) {
                this.bugShots.getChildren()[j].pause();
            }
            if (this.bossSpawned && !this.bossKilled) {
                this.boss.pause();
            }
            for (let k = 0; k < this.powerups.getChildren().length; k++) {
                this.powerups.getChildren()[k].pause();
            }
            this.button = this.add.sprite(config.width / 2, config.height * 1 / 5 + 53, "restartButton")
                .setInteractive()
                .on('pointerdown', () => this.restartGame());
            this.button.depth = 11;

            this.musicVolumeText = this.add.bitmapText(config.width/2, config.height*1/4+95, "pixelFont", "MUSIC VOLUME", 16).setOrigin(0.5);
            this.sfxVolumeText = this.add.bitmapText(config.width/2, config.height*1/4+125, "pixelFont", "SFX VOLUME", 16).setOrigin(0.5);

            this.musicSlider=new Slider(this, config.width/2, config.height*1/4+100, config.width*2/3, this.musicVolume, 1, true);
            this.sfxSlider=new Slider(this, config.width/2, config.height*1/4+130, config.width*2/3, this.loudness, 2, false);
            paused = true;
        } else {
            this.musicVolumeText.destroy();
            this.sfxVolumeText.destroy();
            this.musicSlider.destroy();
            this.sfxSlider.destroy();
            this.unPauseSound.play({volume: this.loudness});
            this.sound.resumeAll();
            this.resumeButton.destroy();
            this.button.destroy();
            this.dim(1);
            this.player.unpause();
            for (let i = 0; i < this.enemyBeetles.getChildren().length; i++) {
                let beetle = this.enemyBeetles.getChildren()[i];
                beetle.unpause();
            }
            for (let j = 0; j < this.bugShots.getChildren().length; j++) {
                this.bugShots.getChildren()[j].unpause();
            }
            if (this.bossSpawned && !this.bossKilled) {
                this.boss.unpause();
            }
            for (let k = 0; k < this.powerups.getChildren().length; k++) {
                this.powerups.getChildren()[k].unpause();
            }
            paused = false;
        }

    }

    prolong() {
        if (paused) {
            this.nextEnemy = this.time.now + this.timeUntilEnemy;
            this.bossTimeStamp = this.time.now + this.timeUntilBoss;
        }
    }

    /**
     * Used to set all values needed for the game
     */
    configure() {
        // creating a new group for beetles to be added to and specifying
        // how frequently they spawn and their speed
        this.enemyBeetles = this.physics.add.group();
        this.bugShots = this.physics.add.group();
        this.nextEnemy = 1000;
        this.enemyRate = 1500;
        this.beetleSpeed = 40;

        this.powerups = this.physics.add.group();

        this.player.configure();

        // setting the score text to show zero
        this.score = 0;
        let scoreFormatted = this.zeroPad(this.score, 6);
        this.scoreLabel.text = "SCORE " + scoreFormatted;

        // adding instruction for the user
        this.instruction1 = this.add.bitmapText(config.width / 2, config.height / 2 - 36, "pixelFont", "DRAG", 32).setOrigin(0.5);
        this.instruction2 = this.add.bitmapText(config.width / 2, config.height / 2 - 12, "pixelFont", "THE PLAYER", 32).setOrigin(0.5);
        this.instruction3 = this.add.bitmapText(config.width / 2, config.height / 2 + 12, "pixelFont", "TO MOVE", 32).setOrigin(0.5);
        this.instruction1.depth = 10;
        this.instruction2.depth = 10;
        this.instruction3.depth = 10;

        // specifying that the game hasn't started yet (until the player moves)
        this.gameStarted = false;
        // specifying that the game is not over yet (until player dies)
        this.gameOver = false;

        // dimming everything in the background of the instructions to 50%
        this.dim(0.5);

        // boss variables
        this.bossKilled = false;
        this.bossSpawned = false;

        this.itemDestroyed = false;
        this.itemPickedUp = false;
        this.itemsSpawned = false;
        this.chosenItem = null;
    }

    /**
     * Setting the alpha value of all existing objects to a specific value
     *
     * @param dimValue the new alpha value for the objects
     */
    dim(dimValue) {
        this.player.alpha = dimValue;
        this.player.dimProjectiles(dimValue);
        this.background.alpha = dimValue;
        this.scoreLabel.alpha = dimValue;
        for (let i = 0; i < this.enemyBeetles.getChildren().length; i++) {
            let beetle = this.enemyBeetles.getChildren()[i];
            beetle.alpha = dimValue;
        }
        for (let j = 0; j < this.bugShots.getChildren().length; j++) {
            this.bugShots.getChildren()[j].alpha = dimValue;
        }
        this.player.lives.alpha = dimValue;
        if (!this.bossKilled && this.bossSpawned) {
            this.boss.dim(dimValue);
            //this.boss.alpha = dimValue;
            for (let k = 0; k < this.boss.errorShots.getChildren().length; k++) {
                this.boss.errorShots.getChildren()[k].alpha = dimValue;
            }
        }
        if (this.itemsSpawned && !this.itemPickedUp) {
            for (let j = 0; j < this.powerups.getChildren().length; j++) {
                this.powerups.getChildren()[j].alpha = dimValue;
            }
        }
    }

    /**
     *  Method to call to start the game and allow the player to move and
     *  enemies to interact
     */
    startGame() {
        this.settings.setVisible(true);
        // exploding all existing beetles behind the instructions
        for (let i = 0; i < this.enemyBeetles.getChildren().length; i++) {
            let beetle = this.enemyBeetles.getChildren()[i];
            new Explosion(this, beetle.x, beetle.y, 1);
        }

        // clearing and destroying all elements in the enemyBeetles group
        this.enemyBeetles.clear(true, true);

        // specifying that the next enemy can spawn in 1 sec
        this.nextEnemy = this.time.now + 1000;

        // specifying the enemy spawn rate and speed
        this.enemyRate = 1500;
        this.beetleSpeed = 70;

        // destroying the instruction objects
        this.instruction1.destroy();
        this.instruction2.destroy();
        this.instruction3.destroy();

        // setting the boolean tracking the games state to started
        this.gameStarted = true;

        // setting all the alpha values back to 1
        this.dim(1);

        this.bossTimeStamp = this.time.now + 15000;

        this.music = this.sound.add("music");

        let musicConfig = {
            mute: false,
            volume: this.musicVolume,
            rate: 1,
            detune: 0,
            seek: 0,
            loop: true,
            delay: 0
        }
        this.music.play(musicConfig);
    }

    /**
     * Method for resetting the game if the player wants to play it from the
     * beginning. Destroys and clears all existing objects after which it
     * will configure them anew.
     */
    resetGame() {

        this.clickSound.play({volume: this.loudness});
        if (!paused) {
            this.button.destroy();
            this.submitButton.destroy();
            this.endMessage.destroy();
            this.endScore.destroy();
        }else{
            this.pauseUnpause();
        }
        this.player.lives.destroy();

        this.player.projectiles.clear(true, true);

        this.enemyBeetles.clear(true, true);
        this.bugShots.clear(true, true);
        if (this.bossSpawned && !this.bossKilled) {
            this.boss.errorShots.clear(true, true);
            this.boss.healthBar.destroy();
            this.boss.destroy();
        }
        for (let i = 0; i < this.scores.length; i++) {
            this.scores[i].destroy();
        }
        this.scores = [];

        this.powerups.clear(true, true);
        this.configure();
    }

    restartGame(){
        this.settings.setVisible(false);
        this.player.resetPlayer();
        this.sound.remove(this.music);
        this.gameOver = true;
        //blocks player from moving
        this.player.playerCanMove = false;
        this.resetGame();

    }

    /**
     * Shows instructions and buttons after the player loses all lives.
     */
    endGame() {
        this.gameOverSound.play({volume: this.loudness});
        this.sound.remove(this.music);
        this.settings.setVisible(false);
        this.getHighScores();
        // showing the score in the end and a game over message
        this.endMessage = this.add.bitmapText(config.width / 2, 30, "pixelFont", "GAME OVER", 30).setOrigin(0.5);
        let scoreFormatted = this.zeroPad(this.score, 6);
        this.endScore = this.add.bitmapText(config.width / 2, 55, "pixelFont", "" + scoreFormatted, 30).setOrigin(0.5);

        // creates a button to restart the game
        this.button = this.add.sprite(config.width / 2, config.height *5/6-39, "restartButton")
            .setInteractive()
            .on('pointerdown', () => this.resetGame());
        this.button.depth = 10;

        // creates a button to submit the score
        this.submitButton = this.add.image(config.width / 2, config.height *5/6+9, "submitButton")
            .setInteractive()
            .on('pointerdown', () => this.submit(this.score));
        this.submitButton.depth = 10;
        this.endMessage.depth = 10;
        this.endScore.depth = 10;

        // sets the gameOver boolean to indicate that the player lost
        this.gameOver = true;
        //blocks player from moving
        this.player.playerCanMove = false;

        //slows down the beetles in the background
        this.beetleSpeed = 30;

        // dims everything in the background
        this.dim(0.3);

        if (this.bossSpawned && !this.bossKilled) this.boss.speed /= 2;
    }

    /**
     * Prints the three daily high scores on the end screen
     */
    getHighScores() {
        let request = new XMLHttpRequest;
        let wholeScene = this;
        request.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                let res = JSON.parse(this.responseText);
                for (let i = 0; i < res.length; i++) {
                    if (res[i] !== null) {
                        let number = wholeScene.add.bitmapText(config.width * 1 / 7, 75 + i * (0.09 * config.width), "pixelFont",
                            (i + 1) + "", (0.11 * config.width));
                        number.setDepth(10);
                        let name = wholeScene.add.bitmapText(config.width * 1 / 7 + (0.077 * config.width), 75 + i * (0.09 * config.width), "pixelFont",
                            "" + res[i].firstName.substr(0, 8), (0.11 * config.width));
                        name.setDepth(10);
                        let score = wholeScene.add.bitmapText(config.width * 1 / 7 + (0.46 * config.width), 75 + i * (0.09 * config.width), "pixelFont",
                            "" + wholeScene.zeroPad(res[i].score, 6), (0.11 * config.width));
                        score.setDepth(10);
                        wholeScene.scores.push(number, name, score);

                    }
                }
            }
        }
        request.open("GET", "http://" + localIp + "/di-OV-1/rest/person", true);
        request.send();
    }

    /**
     * Calls a new scene that can be used to submit the score, returns the
     * dom container transform value to the initial one.
     */
    submit() {

        this.clickSound.play({volume: this.loudness});
        highscore = this.score;
        let div = document.getElementsByTagName('div');
        div[0].style.transform = oldTransform;
        this.scene.start("inputScreen");
    }

    /**
     * Creates padding for a number for the score text
     * @param number the number (in this case score) to be padded
     * @param size the size which the number as a string should have
     * @returns {string} the number as a string padded with 0s to the
     * expected length
     */
    zeroPad(number, size) {
        let stringNumber = String(number);
        while (stringNumber.length < size) {
            stringNumber = "0" + stringNumber;
        }
        return stringNumber;
    }

    addToScore(number) {
        if (this.player.lives.hearts === 0) return;
        this.score += number;
        let scoreFormatted = this.zeroPad(this.score, 6);
        this.scoreLabel.text = "SCORE " + scoreFormatted;
    }

    update() {

        if (paused) {
            this.prolong();
            this.player.prolong();
            for (let i = 0; i < this.enemyBeetles.getChildren().length; i++) {
                this.enemyBeetles.getChildren()[i].prolong();
            }
            if (this.bossSpawned && !this.bossKilled) {
                this.boss.prolong();
            }
            for (let k = 0; k < this.powerups.getChildren().length; k++) {
                this.powerups.getChildren()[k].prolong();
            }
            this.sfxSlider.update();
            this.musicSlider.update();
            return;
        }
        //keeps game dimmed until it is started
        if (this.gameStarted === false) {
            this.dim(0.5);
        }
        if (this.gameOver === true) {
            this.dim(0.3);
        }

        // makes the enemyRate lower and lower with progression until it
        // reaches 500
        if (this.gameStarted && !this.gameOver && this.enemyRate > 400 && this.bossKilled && this.itemDestroyed && !paused) {
            this.enemyRate -= 0.5;
        }
        // moves the background up
        this.background.tilePositionY -= 0.5;

        //causes beetles to spawn
        if (!this.bossSpawned || this.bossSpawned && this.bossKilled && this.itemDestroyed) this.spawnBeetle();
        if (this.gameStarted && !this.bossSpawned && this.time.now > this.bossTimeStamp) {
            this.bossSpawned = true;
            this.boss = new Boss(this);
        }
        if (this.bossSpawned && !this.bossKilled) {
            this.boss.update();
        }
        if (this.bossSpawned && this.bossKilled && !this.itemsSpawned && (this.player.y < config.height * 2 / 5 - 20 || this.player.y > config.height * 2 / 5 + 20)) {
            this.spawnItems();
        } else if (this.itemsSpawned && !this.itemPickedUp) {
            for (let i = 0; i < this.powerups.getChildren().length; i++) {
                this.powerups.getChildren()[i].update();
            }
        } else if (this.itemsSpawned && !this.itemDestroyed) {
            for (let i = this.powerups.getChildren().length - 1; i >= 0; i--) {
                this.powerups.getChildren()[i].update();
            }
        }

        this.player.update();

        // updates the beetles to move or get destroyed if they leave the screen
        for (let i = 0; i < this.enemyBeetles.getChildren().length; i++) {
            let beetle = this.enemyBeetles.getChildren()[i];
            beetle.update();
        }
        for (let j = 0; j < this.bugShots.getChildren().length; j++) {
            this.bugShots.getChildren()[j].update();
        }
    }

    spawnItems() {
        new PowerUp(this, 0);
        new PowerUp(this, 1);
        new PowerUp(this, 2);
        this.itemsSpawned = true;
    }

    /**
     * Spawns beetles at the enemy rate of this scene.
     */
    spawnBeetle() {
        if (paused) return;
        if (this.time.now > this.nextEnemy) {
            if (this.bossKilled === false) {
                new Beetle(this, this.beetleSpeed);
            } else {
                let random = Phaser.Math.Between(0, 100);
                if (random < 30) {
                    new LadyBug(this, this.beetleSpeed);
                } else {
                    new Beetle(this, this.beetleSpeed);
                }
            }
            this.nextEnemy = this.time.now + this.enemyRate;
        }
    }
}