class ErrorShot extends Phaser.GameObjects.Image {
    constructor(scene, boss) {
        let x = boss.x;
        let y = boss.y;
        super(scene, x, y, "error");
        this.setScale(0.8);
        scene.add.existing(this);
        scene.physics.world.enableBody(this);
        this.boss = boss;
        this.hp = 4;
        boss.errorShots.add(this);

        this.body.setCollideWorldBounds(true);
        this.body.setBounce(1);

        let destinationX = scene.player.x;
        let destinationY = scene.player.y;
        let rotation = Phaser.Math.Angle.Between(x, y, destinationX, destinationY);
        this.setRotation(rotation);
        scene.physics.velocityFromRotation(rotation, 80, this.body.velocity);
        this.setRotation(rotation - Phaser.Math.DegToRad(90));


    }

    update(){
        this.angle+=1;
    }

    hurt(projectile) {

        this.scene.hurtBossSound.play({volume:this.scene.loudness});
        this.hp--;
        new Explosion(this.scene, projectile.x, projectile.y, 1);
        if (this.hp === 0) {
            new Explosion(this.scene, this.x, this.y, 3);
            this.kill();
        }
    }

    collideWith() {
        this.hp--;
        if (this.hp === 0) {
            new Explosion(this.scene, this.x, this.y, 3);
            this.kill();
        }
    }

    kill() {
        this.scene.bugDeadSound.play({volume:this.scene.loudness});
        new ErrorShotFragment(this.scene, this.boss, this,
            this.x + Phaser.Math.Between(5,20),
            this.y + Phaser.Math.Between(-10,10), 0.5);
        new ErrorShotFragment(this.scene, this.boss, this,
            this.x + Phaser.Math.Between(-20,-10),
            this.y + Phaser.Math.Between(-20,5), 0.5);
        new ErrorShotFragment(this.scene, this.boss, this,
            this.x + Phaser.Math.Between(-15,15),
            this.y + Phaser.Math.Between(5,20), 0.5);
        this.destroy();
    }
    pause(){
        this.body.setEnable(false);
    }
    unpause(){
        this.body.setEnable();
    }
}