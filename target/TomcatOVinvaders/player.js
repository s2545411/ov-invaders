class Player extends Phaser.Physics.Arcade.Sprite{
    constructor(scene){
        super(scene,config.width / 2, config.height - 64, "player" );
        scene.add.existing(this);
        this.play("thrust");
        scene.physics.world.enableBody(this);
        this.setInteractive();
        this.setCollideWorldBounds();
        this.depth=9;


        this.nextShot = 0;


    }
    configure(){
        // creating a new Hearts object tracking the player's lives and
        // specifying the depth so that it is on top
        this.lives = new Hearts(this.scene);
        this.lives.depth = 10;

        // allowing player to move
        this.playerCanMove = true;

        // creating a new group for projectiles to be added to later and
        // specifying the attack speed and when to start shooting
        this.projectiles = this.scene.add.group();
        this.nextFire = 0;
        this.attackSpeed = 500;
        this.projectileType=0;

        // add an overlap to enable collisions of the player and this bugs shots
        this.scene.physics.add.overlap(this, this.scene.bugShots, this.hurtPlayer, null, this);

        // adding overlap for the beetles to interact with the player on contact
        this.scene.physics.add.overlap(this, this.scene.enemyBeetles, this.hurtPlayer, null, this);

        // adding overlap for the beetles to interact with the projectiles
        // on contact
        this.scene.physics.add.overlap(this.projectiles, this.scene.enemyBeetles, this.hitEnemy, null, this.scene);

        // adding overlap for the beetles to interact with the player on contact
        this.scene.physics.add.overlap(this, this.scene.powerups, this.pickUpPower, null, this);

        this.shotSound=this.scene.shotNormalSound;
        this.vol=2;
        this.powerPicked=false;
    }
    dimProjectiles(dimValue){
        for (let j = 0; j < this.projectiles.getChildren().length; j++) {
            this.projectiles.getChildren()[j].alpha = dimValue;
        }
    }
    pickUpPower(player, power){
        if(this.powerPicked===true)return;
        this.powerPicked=true;
        this.scene.powerupSound.play({volume:this.scene.loudness});
        if(power.language!=0)this.vol=1;
        player.projectileType=1+power.language;
        player.attackSpeed=power.shotSpeeds[power.language];
        player.shotSound=power.shotSounds[power.language];
        power.pickUp();
    }

    /**
     * Causes enemies to take damage when they collide with a projectile,
     * also causing the projectile to disappear and score to go up.
     *
     * @param projectile the projectile that hit the enemy
     * @param enemy the enemy that has been hit
     */
    hitEnemy(projectile, enemy) {
        enemy.hurt(projectile);
        if(projectile.player.projectileType!==1){
            projectile.destroy();
        }
    }

    /**
     * Takes a life from the player, destroys the enemy he hit
     * @param player the player who will lose a life
     * @param enemy the enemy with which the player collided
     */
    hurtPlayer(player, enemy) {
        // will not be executed if the player has not yet moved after being
        // hit for the last time, or game isnt started yet
        if (this.alpha < 1) {
            return;
        }
        this.scene.hurtPlayerSound.play({volume:this.scene.loudness});
        this.lives.decreaseHearts();
        enemy.collideWith();
        // creates an explosion in the place of the player
        new Explosion(this.scene, this.x, this.y, 1);
        // disables collisions with the player
        this.disableBody(true, true);
        // stops the control over the player
        this.stopDrag();
        this.alpha = 0.5;
        this.playerCanMove = false;
        //adds a delay and calls an animation for the players return
        this.scene.time.addEvent({
            delay: 1500,
            callback: this.resetPlayer,
            callbackScope: this,
            loop: false
        });
    }

    /**
     * Animates the players return to the screen and enables collisions again.
     */
    resetPlayer() {
        let x = config.width / 2;
        let y = config.height + 64;
        //enables players collisions
        this.enableBody(true, x, y, true, true);
        // adds an animation for the player to return to the screen
        this.scene.tweens.add({
            targets: this,
            y: config.height * 4 / 5,
            ease: 'Power1',
            duration: 1000,
            repeat: 0,
            onComplete: function () {
                if (this.scene.gameOver === false) this.playerCanMove = true;
            },
            callbackScope: this
        });
    }

    update(){
        this.shoot();

        // will start the drag when the user touches or clicks the screen
        this.scene.input.on('pointerdown', this.startDrag, this);

        // update the shots to move them or destroy them if they leave the
        // screen
        for (let i = 0; i < this.projectiles.getChildren().length; i++) {
            let shot = this.projectiles.getChildren()[i];
            shot.update();
        }
    }

    /**
     * Causes the player to shoot constantly if his alpha value is 1.
     */
    shoot() {
        if (this.alpha < 1||paused) {
            return;
        }
        if (this.active && (this.scene.time.now > this.nextFire)) {
            switch(this.projectileType){
                case 0:
                    new Shot(this.scene, this);
                    break;
                case 1:
                    new SnakeShot(this.scene, this);
                    new SnakeShot(this.scene, this);
                    break;
                case 2:
                    new CoffeeShot(this.scene, this, 0);
                    new CoffeeShot(this.scene, this, 30);
                    new CoffeeShot(this.scene, this, -30);
                    break;
                case 3:
                    new CShot(this.scene, this);
                    break;
            }

            this.shotSound.play({volume:this.scene.loudness*this.vol});
            this.nextFire = this.scene.time.now + this.attackSpeed;
        }
    }

    /**
     * starts dragging the player under the pointer.
     * @param pointer the finger or mouse cursor that the player will track
     */
    startDrag(pointer, targets) {
        // will not execute if the playerCanMove value is false, if he can
        // move but hasn't yet after he has been hit or the game was
        // restarted, he will move and his alpha value will be returned to 1
        if (this.playerCanMove === false||paused||targets[0]!==this) {
            return;
        } else if (this.scene.gameStarted === true) {
            this.alpha = 1;
        }
        // turn off reacting to a pointer down input
        this.scene.input.off('pointerdown', this.startDrag, this);
        // react to moving the pointer or lifting it
        this.scene.input.on('pointermove', this.doDrag, this);
        this.scene.input.on('pointerup', this.stopDrag, this);
    }

    /**
     * Move the player to the pointer while dragging.
     * @param pointer the pointer element, used for the coordinates
     */
    doDrag(pointer) {
        if (this.scene.gameStarted === false) {
            this.scene.startGame();
            this.alpha = 1;
        }
        if (pointer.x < config.width - 10 && pointer.x > 10) this.x = pointer.x;
        if (pointer.y < config.height - 15 && pointer.y > 15) this.y = pointer.y;
    }

    /**
     * Turn off the methods reacting to the pointer being moved or lifted.
     */
    stopDrag() {
        this.scene.input.on('pointerdown', this.startDrag, this);
        this.scene.input.off('pointermove', this.doDrag, this);
        this.scene.input.off('pointerup', this.stopDrag, this);
    }
    pause(){
        this.body.setEnable(false);
        this.timeUntilFire=this.nextFire-this.scene.time.now;
        for (let j = 0; j < this.projectiles.getChildren().length; j++) {
            this.projectiles.getChildren()[j].pause();
        }
    }
    unpause(){
        for (let j = 0; j < this.projectiles.getChildren().length; j++) {
            this.projectiles.getChildren()[j].unpause();
        }
        this.body.setEnable();
    }
    prolong(){
        if(paused){
            this.nextFire=this.scene.time.now+this.timeUntilFire;
        }
    }

}