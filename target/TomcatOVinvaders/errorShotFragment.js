class ErrorShotFragment extends Phaser.GameObjects.Image{
    constructor(scene, boss, errorShot, startX, startY, scale){
        let x=startX;
        let y=startY;
        super(scene, x, y, "error");
        this.setScale(scale);
        scene.add.existing(this);
        scene.physics.world.enableBody(this);

        this.hp=2;
        boss.errorShots.add(this);

        this.body.setCollideWorldBounds(true);
        this.body.setBounce(1);

        let destinationX=2*startX-errorShot.x;
        let destinationY=2*startY-errorShot.y;
        let rotation=Phaser.Math.Angle.Between(x, y, destinationX, destinationY);
        this.setRotation(rotation);
        scene.physics.velocityFromRotation(rotation, 80, this.body.velocity);
        this.setRotation(rotation-Phaser.Math.DegToRad(90));

    }
    update(){
        this.angle+=2;
    }

    hurt(projectile){
        this.scene.hurtBossSound.play({volume:this.scene.loudness});
        this.hp--;
        new Explosion(this.scene,projectile.x, projectile.y, 1);
        if(this.hp===0){
            this.scene.bugDeadSound.play({volume:this.scene.loudness});
            new Explosion(this.scene, this.x, this.y, 3);
            this.destroy();
        }
    }

    collideWith(){
        this.hp--;
        if(this.hp===0){
            new Explosion(this.scene, this.x, this.y, 1);
            this.destroy();
        }
    }
    pause(){
        this.body.setEnable(false);
    }
    unpause(){
        this.body.setEnable();
    }
}