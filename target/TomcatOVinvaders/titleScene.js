class TitleScene extends Phaser.Scene {
    constructor() {
        super("titleScreen");
    }

    create() {
        // Creating the background using the pixel art image
        this.background = this.add.image(0, 0,
            "titleBackground").setScale(0.5).setOrigin(0);

        // Adding the ship in the corner to the image for aesthetics and detail
        this.ship = this.add.image(config.width * 6 / 7, config.height * 6 / 7, "titleShip");
        this.ship.setScale(0.5);

        // Adding the pixel art title image
        this.titleText = this.add.image(config.width / 2, config.height * 1 / 5, "title").setScale(1.25);

        // Adding the button that will call changeScene() on press
        this.button = this.add.image(config.width / 2, config.height * 2.6 / 5,
            "startButton")
            .setInteractive()
            .on('pointerdown', ()=>this.changeScene());
        this.button = this.add.image(config.width / 2, config.height * 3.5 / 5,
            "websiteButton")
            .setInteractive()
            .on('pointerdown', ()=>this.aboutUs());
    }

    // Method changing the scene to Scene2 (called by the name "playGame")
    changeScene() {
        this.scene.start("playGame");
    }
    aboutUs(){
        window.location.href = "https://www.ovsoftware.nl/";
    }
}