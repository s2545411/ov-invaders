class Shot extends Phaser.GameObjects.Image{
    constructor(scene, player){
        let x=player.x-5+player.nextShot*10;
        let y=player.y-17;
        super(scene, x, y, "shot"+player.nextShot);
        // alternate the shots between a 0 image and a 1 image
        player.nextShot=1-player.nextShot;
        scene.add.existing(this);

        // enable movement and collisions
        scene.physics.world.enableBody(this);

        // set the velocity of the shots
        this.body.velocity.y=-220;

        // add this shot to the scenes group of projectiles
        player.projectiles.add(this);

        this.player=player;
    }

    /**
     * Checks if the shots should be destroyed (when they leave the screen).
     */
    update(){
        if(this.y<-5){
            this.destroy();
        }
    }
    pause(){
        this.body.setEnable(false);
    }
    unpause(){
        this.body.setEnable();
    }
}