class Scene1 extends Phaser.Scene{
    constructor(){
        super("bootGame");
    }

    preload(){
        //Shows a text saying that the game is loading
        this.add.text(20,20,"Loading game...");

        //loading all needed images
        this.load.image("background", "assets/img/background1.png");
        this.load.image("shot1", "assets/img/1.png");
        this.load.image("shot0", "assets/img/0.png");
        this.load.image("red4", "assets/img/red4.png");
        this.load.image("red0", "assets/img/red0.png");
        this.load.image("startButton", "assets/img/start.png");
        this.load.image("websiteButton", "assets/img/website.png");
        this.load.image("title", "assets/img/title.png");
        this.load.image("titleBackground", "assets/img/background2.png");
        this.load.image("titleShip", "assets/img/titleShip.png");
        this.load.image("beetleShot", "assets/img/beetleShot.png");
        this.load.image("submitButton", "assets/img/submitButton.png");
        this.load.image("restartButton", "assets/img/restartButton.png");
        this.load.image("resumeButton", "assets/img/resume.png");
        this.load.image("smoothie", "assets/img/smoothie.png");
        this.load.image("error", "assets/img/error.png");
        this.load.image("gear", "assets/img/gear.png");
        this.load.image("c++", "assets/img/c++.png");
        this.load.image("coffee", "assets/img/coffee.png");
        this.load.image("java", "assets/img/java.png");
        this.load.image("python", "assets/img/python.png");
        this.load.image("snake1", "assets/img/snake1.png");
        this.load.image("snake0", "assets/img/snake2.png");

        this.load.audio("music", ["assets/sounds/8bit.ogg", "assets/sounds/8bit.mp3"]);
        this.load.audio("bugDead", "assets/sounds/hurtBug.wav");
        this.load.audio("bugShoot", "assets/sounds/bugShot.mp3");
        this.load.audio("c++shot", "assets/sounds/c++shot.wav");
        this.load.audio("click", "assets/sounds/click.wav");
        this.load.audio("cup", "assets/sounds/cup1.wav");
        this.load.audio("errorShot", "assets/sounds/errorShot.mp3");
        this.load.audio("gameOver", "assets/sounds/gameOver.wav");
        this.load.audio("hurtBoss", "assets/sounds/hurtBoss.mp3");
        this.load.audio("hurtPlayer", "assets/sounds/hurtPlayer.wav");
        this.load.audio("pause", "assets/sounds/pause.mp3");
        this.load.audio("killBoss", "assets/sounds/killBoss.wav");
        this.load.audio("shotNormal", "assets/sounds/shotNormal.wav");
        this.load.audio("snake", "assets/sounds/snake.wav");
        this.load.audio("powerUp", "assets/sounds/powerUp.wav");
        this.load.audio("unpause", "assets/sounds/unpause.wav");
        this.load.audio("win", "assets/sounds/win.wav");

        for(let i=0; i<4;i++){
            this.load.image(i+"hearts", "assets/img/"+i+"hearts.png");
        }

        //loading all needed sprite sheets (need to specify frame width and
        // height for creating animations)
        this.load.spritesheet("player", "assets/ssheets/ovplayer.png", {
            frameWidth: 19,
            frameHeight: 30
        });
        this.load.spritesheet("beetle", "assets/ssheets/beetle.png", {
            frameWidth: 24,
            frameHeight: 20
        });
        this.load.spritesheet("boss", "assets/ssheets/boss.png", {
            frameWidth: 116,
            frameHeight: 74
        });
        this.load.spritesheet("explosion", "assets/ssheets/explosion.png", {
            frameWidth: 16,
            frameHeight: 16
        });
        this.load.spritesheet("ladybug", "assets/ssheets/ladybug.png", {
            frameWidth: 26,
            frameHeight: 23
        });

        //loading the font for the score and text
        this.load.bitmapFont("pixelFont", "assets/font/font.png", "assets/font/font.xml");
    }

    create(){

        //Starting the title screen scene when preloading is finished
        this.scene.start("titleScreen");

        //creating all needed animations, with framerate and number of
        // repeats (-1 means infinity)
        this.anims.create({
            key: "thrust",
            frames: this.anims.generateFrameNumbers("player"),
            frameRate: 20,
            repeat: -1
        });
        this.anims.create({
            key: "boss_anim",
            frames: this.anims.generateFrameNumbers("boss"),
            frameRate: 12,
            repeat: -1
        });
        this.anims.create({
            key: "beetle_walk",
            frames: this.anims.generateFrameNumbers("beetle"),
            frameRate: 3,
            repeat: -1
        });
        this.anims.create({
            key: "ladybug_walk",
            frames: this.anims.generateFrameNumbers("ladybug"),
            frameRate: 3,
            repeat: -1
        });
        this.anims.create({
            key: "explode",
            frames: this.anims.generateFrameNumbers("explosion"),
            frameRate: 20,
            repeat: 0,
            hideOnComplete: true
        });
    }
}