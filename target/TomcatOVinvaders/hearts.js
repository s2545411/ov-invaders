class Hearts extends Phaser.GameObjects.Image {
    constructor(scene) {
        let x = config.width - 25;
        let y = 9;
        super(scene, x, y, "3hearts");
        scene.add.existing(this);
        this.hearts = 3;
    }

    /**
     * Decrease the number of hearts and change the image, if the amount is
     * 0 end the game.
     */
    decreaseHearts() {
        if(this.hearts>1){
            this.setTexture(this.hearts-1 + "hearts");
            this.hearts--;
        }else{
            this.hearts=0;
            this.setTexture(this.hearts + "hearts");
            this.scene.endGame();
        }
    }
}