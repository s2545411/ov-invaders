class LadyBug extends Phaser.GameObjects.Sprite{
    constructor(scene, beetleSpeed){
        // makes the beetle spawn randomly out of the screens upper half,
        let x;
        let y;
        let randomPos=Phaser.Math.Between(0, config.height+config.width+2*40);
        if(randomPos<config.height/2){
            x=-40;
            y=Phaser.Math.Between(0, config.height/2);
        }else if(randomPos<config.height){
            x=config.width+40;
            y=Phaser.Math.Between(0, config.height/2);
        }else if(randomPos<config.height+config.width){
            x=Phaser.Math.Between(0, config.width);
            y=-40;
        }else if(randomPos<config.height+config.width+40){
            x=Phaser.Math.Between(-60, -30);
            y=Phaser.Math.Between(-60, -30);
        }else{
            x=config.width+Phaser.Math.Between(30, 60);
            y=Phaser.Math.Between(-60, -30);
        }

        // saves the destination in the direction of which the beetle will
        // travel
        let destinationX=config.width-x;
        let destinationY=config.height-y;
        // calls the of the sprite super class
        super(scene, x, y, "ladybug");

        // adds the beetle to the scene
        scene.add.existing(this);

        // adds the animation moving the beetles legs
        this.play("ladybug_walk");

        // enable collisions and movement
        scene.physics.world.enableBody(this);

        // add this object to the group of beetles in the scene
        scene.enemyBeetles.add(this);

        // set the rotation based on the objects destination
        let rotation=Phaser.Math.Angle.Between(x, y, destinationX, destinationY);
        this.setRotation(rotation);

        // set the velocity of the beetle
        scene.physics.velocityFromRotation(rotation, beetleSpeed, this.body.velocity);

        // because the sprites were created turned 90 degrees it must be
        // rotated by 90 more degrees
        this.setRotation(rotation-Phaser.Math.DegToRad(90));

        // create a new group for this beetles shots
        this.bugShots=scene.add.group();
        // make beetle start shooting randomly after a period of 200 to 600
        // milliseconds after creation
        this.nextFire=this.scene.time.now+Phaser.Math.Between(200,600);
        this.attackSpeed=1500;

        this.hp=1;
    }

    /**
     * Shoots and checks if the object is out of the screen to destroy him
     */
    update(){
        if(this.y>config.height+20||this.x<-65||this.x>config.width+65){
            this.destroy();
        }
        if(this.y<config.height-64&&this.y>12&&this.x>12&&this.x<config.width-12)this.shoot();

    }

    /**
     * Create new objects of the BeetleShot class at a constant rate if the
     * player is not on a hit cooldown and the game was started
     */
    shoot(){
        if(this.active===false||paused)return;
        if(this.scene.player.alpha>0.5&&this.scene.time.now>this.nextFire){
            this.scene.bugShotSound.play({volume:this.scene.loudness*0.5});
            new LadybugShot(this.scene, this, 0);
            new LadybugShot(this.scene, this, 30);
            new LadybugShot(this.scene, this, -30);
            this.nextFire=this.scene.time.now+this.attackSpeed;
        }else if(this.scene.player.alpha<=0.5){
            this.nextFire=this.scene.time.now+Phaser.Math.Between(500,this.attackSpeed);
        }
    }
    hurt(projectile){
        this.hp--;
        new Explosion(this.scene, projectile.x, projectile.y, 0.5);
        if(this.hp===0){
            this.kill();
        }
    }
    kill(){
        this.scene.bugDeadSound.play({volume:this.scene.loudness*2});
        new Explosion(this.scene, this.x, this.y, 1);
        this.scene.addToScore(35);
        this.destroy();
    }
    collideWith(){
        this.hp--;
        if(this.hp===0){
            this.kill();
        }
    }
    pause(){
        this.body.setEnable(false);
        this.timeUntilFire=this.nextFire-this.scene.time.now;
    }
    unpause(){
        this.body.setEnable();
    }
    prolong(){
        if(paused){
            this.nextFire=this.scene.time.now+this.timeUntilFire;
        }
    }
}