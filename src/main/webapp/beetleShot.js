class BeetleShot extends Phaser.GameObjects.Image{
    constructor(scene, beetle){
        // set the coordinates of this shot to that of the shooting beetle
        let x=beetle.x;
        let y=beetle.y+12;
        super(scene, x, y, "beetleShot");

        // add object to the scene
        scene.add.existing(this);

        // enable movement and collisions
        scene.physics.world.enableBody(this);
        scene.bugShots.add(this);
        // choose the player as the destination, set the rotation and
        // velocity appropriately
        let destinationX=scene.player.x;
        let destinationY=scene.player.y;
        let rotation=Phaser.Math.Angle.Between(x, y, destinationX, destinationY);
        this.setRotation(rotation);
        scene.physics.velocityFromRotation(rotation, 100, this.body.velocity);
        this.setRotation(rotation-Phaser.Math.DegToRad(90));

        // add the shot to the group of shots of the shooting beetle

    }

    /**
     * Destroys the shots that are out of the screen.
     */
    update(){
        if(this.y<-10||this.y>config.height+10||this.x<-10||this.x>config.width+10){
            this.destroy();
        }
    }

    collideWith(){
        this.destroy();
    }
    pause(){
        this.body.setEnable(false);
    }
    unpause(){
        this.body.setEnable();
    }
}