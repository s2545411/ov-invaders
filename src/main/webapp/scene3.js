let showSmoothie;
let smoothie;
let endScene;

class Scene3 extends Phaser.Scene {
    constructor() {
        super("inputScreen");
    }

    preload() {
        // loads the form from which the domelements are taken
        this.load.html("nameform", "assets/text/nameform.html");
    }

    create() {
        this.scale.updateBounds();
        let div1 = document.getElementsByTagName('div');
        div1[0].style.transform = "scale(1)";
        div1[0].style.width = config.width*config.zoom+"px";
        div1[0].style.height = config.height*config.zoom+"px";

        endScene = this;
        showSmoothie = false;
        // sets the background like in scene 1
        this.background = this.add.image(0, 0,
            "titleBackground").setScale(0.5).setOrigin(0);
        this.ship = this.add.image(config.width * 6 / 7, config.height * 6 / 7, "titleShip");
        this.ship.setScale(0.5);
        // adds the smoothie image, but makes it invisible for not
        smoothie = this.add.image(config.width / 2, config.height / 2 + 60, "smoothie");
        smoothie.setVisible(false);
        // sets the variable of the smoothie's rotation and scale change
        this.smoothieScale = 1;
        this.smoothieChange = 0.004;
        this.smoothieRotationChange = 0.5;
        // adds the dom elements (the ui) from the html object
        let form = this.add.dom(config.width / 2, -config.height*6).createFromCache("nameform").setOrigin(0.05);
        this.form = form;
        this.form.setScale(1);
        // alert("originX= " + form.originX + "  originY= " + form.originY + "  height=" +
        //     " " + form.y);
        let sceneObj = this;
        form.depth = 10;

        // let styleSheet=document.styleSheets[1];
        // for (let i = 0; i < styleSheet.rules.length; i++) {
        //     // if you are looking for selector '.main'
        //     if (styleSheet.rules[i].selectorText === 'label') {
        //         // set background color
        //         styleSheet.rules[i].style.fontSize = config.height*0.005+'px';
        //     }
        //     if (styleSheet.rules[i].selectorText === 'p') {
        //         // set background color
        //         styleSheet.rules[i].style.fontSize = config.height*0.0037+'px';
        //     }
        // }

        // add listener for clicking the submit button.
        let submitButton = this.add.image(config.width / 2, 0, "submitButton")
            .setInteractive()
            .on('pointerdown', function () {

                // save the users inputs
                let fname = form.getChildByName("firstname");
                let lname = form.getChildByName("lastname");
                let email = form.getChildByName("email");
                let nletter = form.getChildByName("newsletter");

                // if the inputs are not null, removes the ui and submits
                // the information
                if (fname.value !== "" && lname.value !== "" && email.value !== "") {
                    let fnameUpper = fname.value.toUpperCase();
                    let lnameUpper = lname.value.toUpperCase();
                    let emailUpper = email.value.toUpperCase();
                    if (sceneObj.isName(fnameUpper) && sceneObj.isName(lnameUpper) && sceneObj.isEmail(emailUpper)) {
                        this.removeListener("pointerdown");
                        form.setVisible(false);
                        this.setVisible(false);
                        sceneObj.submitInformation(fname.value.toUpperCase(), lname.value.toUpperCase(), email.value.toUpperCase(), nletter.checked, highscore);
                    } else {
                        if (!sceneObj.isName(fnameUpper)) {
                            fname.value = '';
                            alert('The name can only contain symbols a-z, -,' +
                                ' and a \'');
                        }
                        if (!sceneObj.isName(lnameUpper)) {
                            lname.value = '';
                            alert('The last name can only contain symbols' +
                                ' a-z, -,' +
                                ' and a \'');
                        }
                        if (!sceneObj.isEmail(emailUpper)) {
                            email.value = '';
                            alert('Invalid email');
                        }
                    }
                } else {
                    alert('Please do not leave the name, last name or email' +
                        ' empty!');
                }

            });
        // adds an animation to move the form into the screen
        this.tween = this.tweens.add({
            targets: form,
            y: (config.height * 25 / 50),
            duration: 3000,
            ease: 'Power3',
            onComplete: function () {
                //alert("height= " + (form.y));
            }
        });
        this.tweens.add({
            targets: submitButton,
            y: config.height * 5 / 6 + 10,
            duration: 3000,
            ease: 'Power3'
        });
    }

    isName(name) {
        let nameWhiteList = "ABCDEFGHIJKLMNOPQRSTUVWXYZ -'";
        for (let i = 0; i < name.length; i++) {
            if (!nameWhiteList.includes(name.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    isEmail(email) {
        let emailWhiteList = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@.#$%&'*+-/=?^_`{|}~";
        let containsAt = false;
        let containsDot = false;
        let containsSpecial = false;
        for (let i = 0; i < email.length; i++) {
            if (email.charAt(i) === '@') {
                if (!containsAt) {
                    containsAt = true;
                } else {
                    return false;
                }
            }
            if (containsAt && email.charAt(i) === '.') {
                containsDot = true;
            }
            if ((i === 0 || i === email.length - 1) && (email.charAt(i) === '.')) {
                return false;
            }
        }

        for (let i = 0; i < email.length; i++) {
            if (!emailWhiteList.includes(email.charAt(i))) {
                containsSpecial = true;
            }
        }
        return containsAt && containsDot && !containsSpecial;

    }

    /**
     * Submits the information of a user to the databse.
     * @param fname
     * @param lname
     * @param email
     * @param nletter
     * @param score
     */
    submitInformation(fname, lname, email, nletter, score) {
        // create a new request to the server
        let request = new XMLHttpRequest();
        // start showing some text and the smoothie after successfully
        // adding a person
        request.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                endScene.thanks = endScene.add.bitmapText(config.width / 2, config.height / 2 - 115, "pixelFont", "THANK YOU", 24).setOrigin(0.5);
                endScene.thanks2 = endScene.add.bitmapText(config.width / 2, config.height / 2 - 100, "pixelFont", "FOR PLAYING!", 24).setOrigin(0.5);
                if (this.responseText === "true") {
                    showSmoothie = true;
                    endScene.claim = endScene.add.bitmapText(config.width / 2, config.height / 2 - 80, "pixelFont", "YOU CAN NOW", 16).setOrigin(0.5);
                    endScene.claim2 = endScene.add.bitmapText(config.width / 2, config.height / 2 - 70, "pixelFont", "CLAIM YOUR SMOOTHIE!", 16).setOrigin(0.5);
                    endScene.updateInfo1 = endScene.add.bitmapText(config.width / 2, config.height / 2 - 50, "pixelFont", "To update your score,", 16).setOrigin(0.5);
                    endScene.updateInfo2 = endScene.add.bitmapText(config.width / 2, config.height / 2 - 40, "pixelFont", "enter the same", 16).setOrigin(0.5);
                    endScene.updateInfo3 = endScene.add.bitmapText(config.width / 2, config.height / 2 - 30, "pixelFont", "info next time", 16).setOrigin(0.5);
                    smoothie.setVisible(true);
                } else {
                    endScene.yourScore = endScene.add.bitmapText(config.width / 2, config.height / 2 - 50, "pixelFont", "YOUR SCORE WAS UPDATED", 16).setOrigin(0.5);
                    endScene.unfortunately = endScene.add.bitmapText(config.width / 2, config.height / 2, "pixelFont", "UNFORTUNATELY,", 16).setOrigin(0.5);
                    endScene.already = endScene.add.bitmapText(config.width / 2, config.height / 2 + 15, "pixelFont", "YOU HAVE ALREADY", 16).setOrigin(0.5);
                    endScene.claimed = endScene.add.bitmapText(config.width / 2, config.height / 2 + 30, "pixelFont", "CLAIMED THE SMOOTHIE", 16).setOrigin(0.5);
                }
            } else if (this.readyState === 4) {
                endScene.error = endScene.add.bitmapText(config.width / 2, config.height / 2 - 40, "pixelFont", "AN ERROR", 24).setOrigin(0.5);
                endScene.occurred = endScene.add.bitmapText(config.width / 2, config.height / 2 - 20, "pixelFont", "OCCURRED :(", 24).setOrigin(0.5);
                endScene.sorry = endScene.add.bitmapText(config.width / 2, config.height / 2 + 20, "pixelFont", "SORRY!", 24).setOrigin(0.5);
            }
        }
        // set the method to a post method, set the url
        request.open("POST", "http://" + localIp + "/di-OV-1/rest/person", true);
        // specifying that the Person as a JSON objects will be sent and
        // setting the body appropriately
        request.setRequestHeader("Content-Type", "application/json");
        let json = {
            "firstName": fname,
            "lastName": lname,
            "email": email,
            "newsletter": nletter,
            "score": score
        };
        //console.log(JSON.stringify(json));
        // sending request
        request.send(JSON.stringify(json));
    }

    /**
     * Shows the smoothie image rotating and changing scale after submitting
     * data.
     */
    update() {
        if (showSmoothie) {
            smoothie.setScale(this.smoothieScale);
            if (this.smoothieScale >= 1.2) {
                this.smoothieChange = -0.004;
            } else if (this.smoothieScale <= 1) {
                this.smoothieChange = 0.004;
            }
            this.smoothieScale += this.smoothieChange;
            if (smoothie.angle >= 50) {
                //console.log("rot: "+smoothie.angle+"  scale:
                // "+this.smoothieScale);
                this.smoothieRotationChange = -0.5;
            } else if (smoothie.angle <= -50) {
                //console.log("rot: "+smoothie.angle+"  scale:
                // "+this.smoothieScale);
                this.smoothieRotationChange = 0.5;
            }
            smoothie.angle += this.smoothieRotationChange;
        }
    }
}