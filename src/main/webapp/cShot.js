class CShot extends Shot{
    constructor(scene, player){
        super(scene, player);
        this.x=player.x+Phaser.Math.Between(-5, 5);
        let angle=Phaser.Math.Between(-20, 20);
        this.setScale(2);
        let rotation=Phaser.Math.Angle.Between(this.x, this.y, this.x, this.y-20)+Phaser.Math.DegToRad(angle);
        this.setRotation(rotation);
        scene.physics.velocityFromRotation(rotation, 220, this.body.velocity);
        this.setRotation(rotation+Phaser.Math.DegToRad(90));
    }
}