class LadybugShot extends Phaser.GameObjects.Image{
    constructor(scene, ladybug, angle){
        // set the coordinates of this shot to that of the shooting beetle
        let x=ladybug.x;
        let y=ladybug.y+12;
        let img="red4";
        if(angle===0)img="red0";

        super(scene, x, y, img);

        // add object to the scene
        scene.add.existing(this);

        // enable movement and collisions
        scene.physics.world.enableBody(this);
        // add the shot to the group of shots of the shooting beetle
        scene.bugShots.add(this);
        // choose the player as the destination, set the rotation and
        // velocity appropriately
        let destinationX=scene.player.x;
        let destinationY=scene.player.y;
        let rotation=Phaser.Math.Angle.Between(x, y, destinationX, destinationY)+Phaser.Math.DegToRad(angle);
        this.setRotation(rotation);
        scene.physics.velocityFromRotation(rotation, 120, this.body.velocity);
        this.setRotation(rotation-Phaser.Math.DegToRad(90));


    }

    /**
     * Destroys the shots that are out of the screen.
     */
    update(){
        if(this.y<-10||this.y>config.height+10||this.x<-10||this.x>config.width+10){
            this.destroy();
        }
    }

    collideWith(){
        this.destroy();
    }
    pause(){
        this.body.setEnable(false);
    }
    unpause(){
        this.body.setEnable();
    }
}