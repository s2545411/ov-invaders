class Boss extends Phaser.GameObjects.Sprite{
    constructor(scene){
        let x=config.width/2;
        let y=-40;
        super(scene, x, y, "boss");
        this.destinationY=config.height*1/4;
        this.setScale(1);
        scene.add.existing(this);
        this.play("boss_anim");

        scene.physics.world.enableBody(this);
        this.hp=30;
        this.speed=40;
        this.errorShots=scene.physics.add.group();
        this.nextFire=0;
        this.attackSpeed=10000;

        scene.physics.add.overlap(scene.player, this, scene.player.hurtPlayer, null, scene.player);
        scene.physics.add.overlap(scene.player, this.errorShots, scene.player.hurtPlayer, null, scene.player);
        scene.physics.add.overlap(scene.player.projectiles, this.errorShots, scene.player.hitEnemy, null, scene.player);
        scene.physics.add.overlap(scene.player.projectiles, this, scene.player.hitEnemy, null, scene.player);

        this.healthBar=new HealthBar(scene, this.hp);
    }

    update(){
        this.shoot();
        if(this.y<this.destinationY*9/10){
            this.y+=(this.destinationY-this.y)*0.02;
        }else if(this.y>=this.destinationY*8/9&&this.y<this.destinationY){
            this.y=this.destinationY;
            this.body.velocity.x=this.speed;
        } else{
            this.moveSideways();
        }
        for (let i = 0; i < this.errorShots.getChildren().length; i++) {
            let shot = this.errorShots.getChildren()[i];
            shot.update();
        }
    }

    moveSideways(){
        if(this.x>config.width*7/8){
            this.body.velocity.x=-this.speed;
        }
        if(this.x<config.width*1/8){
            this.body.velocity.x=this.speed;
        }
    }

    shoot(){

        if(this.active===false||paused)return;
        if((this.scene.player.alpha>0.5||this.y<this.destinationY)&&this.scene.time.now>this.nextFire){
            new ErrorShot(this.scene, this);
            this.scene.errorSound.play({volume:this.scene.loudness});
            this.nextFire=this.scene.time.now+this.attackSpeed;
        }else if(this.scene.player.alpha<=0.5){
            this.nextFire=this.scene.time.now+this.attackSpeed*3/4;
        }
    }
    prolong(){
        if(paused){
            this.nextFire=this.scene.time.now+this.timeUntilFire;
        }
    }

    hurt(projectile){
        this.hp--;
        this.scene.hurtBossSound.play({volume:this.scene.loudness});
        new Explosion(this.scene,projectile.x, projectile.y, 1);
        if(this.hp===0){
            this.kill();
        }
        this.healthBar.decrease();
    }

    collideWith(){
        this.hp--;
        if(this.hp===0){
            this.kill();
        }
    }
    kill(){
        this.scene.killBossSound.play({volume:this.scene.loudness});
        for(let i=0;i<20;i++){
            new Explosion(this.scene, this.x+Phaser.Math.Between(-80, 80), this.y+Phaser.Math.Between(-35, 35), 3);
        }
        this.scene.bossKilled=true;
        this.errorShots.clear(true, true);
        this.scene.addToScore(1000);
        this.healthBar.destroy();
        this.scene.winSound.play({volume:this.scene.loudness});
        this.destroy();
    }
    dim(dimValue){
        this.alpha=dimValue;
        this.healthBar.dim(dimValue);
    }
    pause(){
        this.body.setEnable(false);

        this.timeUntilFire=this.nextFire-this.scene.time.now;

        for (let j = 0; j < this.errorShots.getChildren().length; j++) {
            this.errorShots.getChildren()[j].pause();
        }
    }
    unpause(){
        for (let j = 0; j < this.errorShots.getChildren().length; j++) {
            this.errorShots.getChildren()[j].unpause();
        }
        this.body.setEnable();
    }
}