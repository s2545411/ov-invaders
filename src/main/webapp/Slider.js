class Slider {
    constructor(scene, x, y, size, value, range, isMusic)
    {
        this.x=x;
        this.y=y;
        this.size=size;
        this.bar = new Phaser.GameObjects.Graphics(scene);
        this.value = value;
        this.range=range;
        this.isMusic=isMusic;
        this.scene=scene;
        this.draw();
        scene.add.existing(this.bar);
    }

    draw ()
    {
        this.bar.clear();
        this.bar.lineStyle(2, 0x33385c)
        this.bar.strokeRect(this.x-this.size/2-2, this.y, this.size+4, 9)
        this.bar.fillStyle(0xf4c800);
        this.bar.fillRect(this.x-this.size/2-1, this.y+1, this.size*this.value/this.range, 7);
    }
    destroy(){
        this.bar.destroy();
    }

    /**
     * starts dragging the player under the pointer.
     * @param pointer the finger or mouse cursor that the player will track
     */
    startDrag(pointer, targets) {
        // will not execute if the playerCanMove value is false, if he can
        // move but hasn't yet after he has been hit or the game was
        // restarted, he will move and his alpha value will be returned to 1
        if (!paused) {
            return;
        }
        // turn off reacting to a pointer down input
        this.scene.input.off('pointerdown', this.startDrag, this);
        // react to moving the pointer or lifting it
        this.scene.input.on('pointermove', this.doDrag, this);
        this.scene.input.on('pointerup', this.stopDrag, this);
    }

    /**
     * Move the player to the pointer while dragging.
     * @param pointer the pointer element, used for the coordinates
     */
    doDrag(pointer) {
        if (pointer.x < this.x+this.size/2+2 && pointer.x >= this.x-this.size/2-2 && pointer.y<=this.y+11&&pointer.y>=this.y) {
            this.value = this.range*(pointer.x-(this.x-this.size/2-1))/this.size;
            if(this.value<0)this.value=0;
            if(this.isMusic){
                this.scene.musicVolume=this.value;
            }else{
                this.scene.loudness=this.value;
            }
            this.draw();
        }
    }

    /**
     * Turn off the methods reacting to the pointer being moved or lifted.
     */
    stopDrag() {
        if(this.isMusic){
            this.scene.music.volume=this.value;
        }
        this.scene.input.on('pointerdown', this.startDrag, this);
        this.scene.input.off('pointermove', this.doDrag, this);
        this.scene.input.off('pointerup', this.stopDrag, this);
    }

    update(){
        this.scene.input.on('pointerdown', this.startDrag, this);
    }
}