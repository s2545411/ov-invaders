class SnakeShot extends Shot{
    constructor(scene, player){
        super(scene, player);
        switch(player.nextShot){
            case 0:
                this.setTexture("snake0");
                break;
            case 1:
                this.setTexture("snake1");
                break;
        }
        this.x+=(this.x-player.x)*2;
        let angle=-15+player.nextShot*30;
        let rotation=Phaser.Math.Angle.Between(this.x, this.y, this.x, this.y-20)+Phaser.Math.DegToRad(angle);
        this.setRotation(rotation);
        scene.physics.velocityFromRotation(rotation, 220, this.body.velocity);
        this.setRotation(rotation+Phaser.Math.DegToRad(90));
    }
}