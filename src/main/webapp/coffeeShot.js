class CoffeeShot extends Shot{
    constructor(scene, player, angle){
        super(scene, player);
        this.setTexture("coffee");

        this.body.setCollideWorldBounds(true);
        this.body.setBounce(1);
        this.body.onWorldBounds=true;
        this.body.world.on('worldbounds', function(body) {
                body.setCollideWorldBounds(false);
                body.onWorldBounds=false;
        }, this);

        this.x=player.x;
        let rotation=Phaser.Math.Angle.Between(this.x, this.y, this.x, this.y-20)+Phaser.Math.DegToRad(angle+Phaser.Math.Between(-10, 10));
        this.setRotation(rotation);
        scene.physics.velocityFromRotation(rotation, 260, this.body.velocity);
        this.setRotation(rotation+Phaser.Math.DegToRad(90));
    }
    update(){
        this.angle+=3;
        if(this.y<-10||this.y>config.height+10||this.x<-10||this.x>config.width+10){
            this.destroy();
        }
    }
}