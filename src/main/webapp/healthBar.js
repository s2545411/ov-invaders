class HealthBar {
    constructor(scene, value)
    {
        this.bar = new Phaser.GameObjects.Graphics(scene);
        this.value = value;
        this.initialValue=value;
        this.draw();
        scene.add.existing(this.bar);
    }

    decrease()
    {
        this.value --;
        if (this.value < 0)
        {
            this.value = 0;
        }
        this.draw();
    }

    draw ()
    {
        this.bar.clear();

        //  BG
        //this.bar.fillStyle(0x346e87);
        //this.bar.fillRect(config.width/2-this.initialValue-2, 10,
        //2*this.initialValue+4, 12);
        this.bar.lineStyle(1, 0x346e87, this.bar.alpha)
        this.bar.strokeRect(config.width/2-this.initialValue-2, 15, 2*this.initialValue+4, 9)
        //  Health

        this.bar.fillStyle(0xc93e3e);
        this.bar.fillRect(config.width/2, 16, this.value, 8);
        this.bar.fillRect(config.width/2-this.value, 16, this.value, 8);
    }
    destroy(){
        this.bar.destroy();
    }
    dim(dimValue){
        this.bar.alpha=dimValue;
    }
}