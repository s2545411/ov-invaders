let config;
let game;
let localIp="ov-software-1.paas.hosted-by-previder.com";
//Initializing the config and game
window.onload = function () {
    //settings to use when in vertical mode
    if (innerWidth < innerHeight) {
        config = {
            type: Phaser.CANVAS,
            parent: 'phaser-body',  //the parent of the game will be the
            // element of the html with id phaser-body, this is necessary to
            // use DOM Elements with phaser
            width: innerWidth / (innerHeight / (272)),
            height: 272,            //this is the original height of the
            // background, we
            // want to zoom instead of setting the value to the screen height
            backgroundColor: 0x000000,
            dom: {
                createContainer: true   //setting used in phaser to enable
                // creating dom elements (will create a container for them)
            },
            scene: [Scene1, Scene2, TitleScene, Scene3],    //specifying all
            // scenes (Scene1 will be called first by the game
            pixelArt: true,         // the pixel art setting will make sprites
            // pixelating even after rotation
            zoom: innerHeight / 272,
            physics: {              // the physics element is used for
                // collisions etc.
                default: "arcade",
                arcade: {
                    debug: false
                }
            }
        };
    } else {
        config = {
            type: Phaser.AUTO,
            parent: 'phaser-body',
            width: 160,
            height: 272,
            backgroundColor: 0x000000,
            dom: {
                createContainer: true
            },
            scene: [Scene1, Scene2, TitleScene, Scene3],
            pixelArt: true,
            zoom: innerHeight / 272,
            physics: {
                default: "arcade",
                arcade: {
                    debug: false
                }
            }
        };
        alert("For the optimal experience, we recommend you play on the" +
            " aspect ratio where width<height!");
    }

    //creating the game using the specified configuration
    game = new Phaser.Game(config);
}