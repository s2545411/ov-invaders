class PowerUp extends Phaser.GameObjects.Sprite{
    constructor(scene, languageIndex){
        // language 1 is python
        // language 2 is java
        // language 3 is c++
        let languages=['python', 'java', 'c++'];
        super(scene, config.width*(languageIndex*2+1)/6, config.height*2/5, languages[languageIndex]);
        scene.add.existing(this);
        scene.physics.world.enableBody(this);
        scene.powerups.add(this);
        this.languages=languages;
        this.language=languageIndex;
        this.languageDescriptions=['Sssnakesss!', 'Need your coffee?', 'The' +
        ' fastest language?'];
        this.shotSpeeds=[400, 750, 150];
        this.shotSounds=[this.scene.snakeSound, this.scene.cupSound, this.scene.cShotSound];
        this.itemScale=1;
        this.scaleChange=0.01;
        this.pickedUp=false;
        this.textMoved=false;
        this.timeToDisappear=0;
    }
    update(){
        if(paused)return;
        if(this.scene.itemPickedUp&&this.scene.chosenItem!==this){
            this.destroy();
            return;
        }
        if(this.pickedUp){
            if(!this.textMoved&&this.scene.time.now>this.timeToDisappear){
                this.moveText();
            } else if(this.textMoved&&this.scene.time.now>this.textMovedTime+1000){
                this.removeText();
            }

        }
        if(!this.active)return;
        this.setScale(this.itemScale);
        if(this.itemScale>=1.2){
            this.scaleChange=-0.004;
        }else if(this.itemScale<=1){
            this.scaleChange=0.004;
        }
        this.itemScale+=this.scaleChange;
    }
    pickUp(){
        if(this.pickedUp)return;
        this.pickedUp=true;
        this.scene.chosenItem=this;
        this.scene.itemPickedUp=true;
        this.setVisible(false);
        this.timeToDisappear=this.scene.time.now+3500;
        this.itemText=this.scene.add.bitmapText(-config.width/2, config.height*1/4, "pixelFont",
            this.languages[this.language].toUpperCase(), 28).setOrigin(0.5);
        this.itemText.depth=10;
        this.itemDescription=this.scene.add.bitmapText(-config.width/2, config.height*1/4+20, "pixelFont",
            this.languageDescriptions[this.language], 16).setOrigin(0.5);
        this.itemDescription.depth=10;
        this.scene.tweens.add({
            targets: this.itemText,
            x: config.width/2,
            duration: 1000,
            ease: 'Power3'
        });
        this.scene.tweens.add({
            targets: this.itemDescription,
            x: config.width/2,
            duration: 1000,
            ease: 'Power3'
        });
    }
    moveText(){
        this.scene.tweens.add({
            targets: this.itemText,
            x: config.width*3/2,
            duration: 1000,
            ease: 'Power3'
        });
        this.scene.tweens.add({
            targets: this.itemDescription,
            x: config.width*3/2,
            duration: 1000,
            ease: 'Power3'
        });
        this.textMoved=true;
        this.textMovedTime=this.scene.time.now;
    }
    removeText(){
        this.itemText.destroy();
        this.itemDescription.destroy();
        this.scene.itemDestroyed=true;
        this.destroy();
    }
    pause(){
        this.body.setEnable(false);
        this.timeUntilDisappear=this.timeToDisappear-this.scene.time.now;
        this.textMovedTime=this.scene.time.now;
    }
    unpause(){
        this.body.setEnable();
    }
    prolong(){
        if(paused){
            this.timeToDisappear=this.scene.time.now+this.timeUntilDisappear;
        }
    }
}