package main.java.contactsAndScores.resources;

import contactsAndScores.model.Person;
import contactsAndScores.dao.Persons;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("person")
public class PersonResource {

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public boolean createPerson(Person person) {
        return Persons.instance.addPersonScore(person.getFirstName(),
                person.getLastName(), person.getEmail(),
                person.isNewsletter(), person.getScore());
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Person[] getTop3(Person person){
        return Persons.instance.getBest();
    }
}
