package main.java.contactsAndScores.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Person {
    private String firstName;
    private String lastName;
    private String email;
    private boolean newsletter;
    private int score;

    public Person(){

    }

    public Person(String firstName, String lastName, String email, int score,
                  boolean newsletter){
        setFirstName(firstName);
        setLastName(lastName);
        setEmail(email);
        setNewsletter(newsletter);
    }

    public String getFirstName(){return firstName;}
    public String getLastName(){return lastName;}
    public String getEmail(){return email;}
    public boolean isNewsletter(){return newsletter;}
    public void setFirstName(String firstName){ this.firstName=firstName;}
    public void setLastName(String lastName){this.lastName=lastName;}
    public void setEmail(String email){this.email=email;}
    public void setNewsletter(boolean newsletter){this.newsletter=newsletter;}

    public int getScore() { return score; }

    public void setScore(int score) { this.score = score; }
}
